/*
  本文件是关于C语言学习的第三份作业，目的是检验对于C语言中动态内存管理方式的掌握程度，这次作业量不大，请认真查阅学习相关知识点，完成作业！
  作业形式：
    本次作业只允许填补代码，不能修改原有代码！
    单独编译每一道题目时，打开注释即可
  作业要求：
    正确编译通过程序，并得到正确的结果
  参考教程：
    http://www.runoob.com/cprogramming/c-tutorial.html
    https://www.cnblogs.com/tuhooo/p/7221136.html
		
  Ubuntu 下打开终端执行：
  g++ ./03_main.cpp -o main
  ./main

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

#define DEBUG true 

const bool debug = false; 


void* generateIntArray(int size)
{
  /* TODO::在这里填写你的代码, 全部初始化为0*/

}

float* generateFloatArray(int size)
{
  /* TODO::在这里填写你的代码，全部初始化为-1*/

}

int* remove(int* array, int size, int index)
{
  /* TODO::在这里填写你的代码
  - array : 输入数组
  - size : 输入数组的长度
  - index : 要删除的元素的索引 */
  
}

int* insert(int* array, int size, int pos, int value)
{
  /* TODO::在这里填写你的代码
  - array : 输入数组
  - size : 输入数组的长度
  - pos : 插入元素的位置 
  - value : 要插入的元素值 */
  
}

int* append(int* array1, int array1_size, int* array2, int array2_size)
{
  /* TODO::在这里填写你的代码
  - array1 : 输入数组1
  - array1_size : 数组1的长度
  - array2 : 输入数组2
  - array2_size : 数组2的长度 */
  
}



int main()
{
/*
*********************************************************
** TODO::Question1  内存管理函数的使用
void* calloc(int num, int size);
void free(void *address);  
*********************************************************
*/  
  int* a = (int*) calloc(1, sizeof(int));
  printf("*a is %d\n", ); // calloc 函数默认分配的值
  *a = 200;
  printf("*a is %d\n", );
  free(a);                  // 释放指针指向的内容
  printf("*a is %d\n", ); // 观察释放后 *a 的值
  printf("a is %p\n", );   // 观察释放后指针变量 a 的值
  a = NULL;                 // free 函数仅仅释放了指针变量指向的内存区域，而指针变量本身的值还存在，即 a 成了野指针，最好的方式是在调用完 free 函数后，使 a = NULL，令其为空指针

/*
*********************************************************
** TODO::Question2  内存管理函数的使用
void* malloc(int num);
void* memset(void* address, int c, size_t n) 注意：以字节为单位设置
void free(void *address);  
*********************************************************
*/

/*
  int* va = (int*) malloc(5*sizeof(int));
  memset(va, 0, 5*sizeof(int));           // 初始化为 0
  for(int i = 0; i < 5; i++) {
    printf("va[%d] is %d\n", i, *va);
    va++;
  }
  // TODO:: 将数组va的每个元素初始化为 1
  // 在下面区域填写你的代码
  
  free(va);
  va = NULL;
*/

/*
*********************************************************
** TODO::Question3  动态数组创建，数组的长度是变量
*********************************************************
*/

/*
  int SIZE = 10;
  int* vb = (int*) generateIntArray(SIZE); // 创建长度为 SIZE 的 Int 数组
  float* vc = (float*) generateFloatArray(SIZE);  // 创建长度为 SIZE 的 Float 数组
  for(int i = 0; i < SIZE; i++)
  {
    printf("vc[%d] is %f\n", i, *vc);
    vc++;
  }
  vc -= SIZE;
  free(vb);
  free(vc);
  vb = NULL;
  vc = NULL;
*/

/*
*********************************************************
** TODO::Question4  内存拷贝函数
void *memcpy(void *dest, const void *src, size_t n);
*********************************************************
*/

/*
  int vd[6] = {21, 22, 23, 24, 25};
  int* ve = (int*) generateIntArray(8);
  // TODO::补全memcpy函数
  memcpy(, , );
  for(int i = 0; i < 8; i++)
  {
    printf("ve[%d] is %d\n", i, ve[i]);
  }
  free(ve);
  ve = NULL;
*/  
  printf("---------------------------------------------\n");  
/*
*********************************************************
** TODO::Question5  删除数组元素
*********************************************************
*/

/*
  int vf[6] = {31, 32, 33, 34, 35, 36};
  int* vf_ = remove(vf, 6, 2); // 删除指定位置的元素，并返回数组指针
  
  int sum = 0;
  for(int i = 0; i < 5; i++)
  {
    printf("vf_[%d] is %d\n", i, vf_[i]);
    sum += vf_[i];
  } 
  printf("sum is %d\n", sum);
  if(sum != 168) {
  	printf("结果错误！\n");
  	return 0;
  }
  free(vf_);
  vf_ = NULL;
  printf("---------------------------------------------\n");
*/  
/*
*********************************************************
** TODO::Question6  在数组指定位置插入一个值
*********************************************************
*/

/*
  int vg[6] = {41, 42, 43, 44, 45, 46};
  int value = 100;
  int* vg_ = insert(vg, 6, 2, 100);  // 在 vg 数组的第 2 个位置插入 100
  
  int sum_vg = 0;
  for(int i = 0; i < 7; i++)
  {
    printf("vg_[%d] is %d\n", i, vg_[i]);
    sum_vg += vg_[i];
  } 
  printf("sum_vg is %d\n", sum_vg);
  if(sum_vg != 361) {
  	printf("结果错误！\n");
  	return 0;
  }
  free(vg_);
  vg_ = NULL;
  printf("---------------------------------------------\n"); 
*/

/*
*********************************************************
** TODO::Question7  拼接两个数组
*********************************************************
*/

/*
  int vh[3] = {31, 32, 33};
  int vi[4] = {41, 42, 43, 44};
  
  int* vj = append(vh, 3, vi, 4); // 拼接两个数组返回一个新的数组

  int sum_vj = 0;
  for(int i = 0; i < 7; i++)
  {
    printf("vj[%d] is %d\n", i, vj[i]);
    sum_vj += vj[i];
  } 
  printf("sum_vj is %d\n", sum_vj);
  if(sum_vj != 266) {
  	printf("结果错误！\n");
  	return 0;
  } else {
  	printf("恭喜你完成本次作业！\n");
  }
  free(vj);
  vj = NULL;
  printf("---------------------------------------------\n");
*/  
  return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


