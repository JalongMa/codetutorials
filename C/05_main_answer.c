/*
  本文件是关于C语言学习的第五份作业，目的是检验对于C语言中字符串等其它技术的掌握程度。
  本次练习是C语言的最后一次作业，基本上五次练习已经涵盖了C语言的绝大部分内容，当然还有C语言还有很多技术以及技巧需要大家掌握，剩下的知识点望大家在实际的C语言项目中学习总结！
  大家应该可以看到，C语言的系列练习前四次作业中，我有意的在回避使用字符变量以及字符串，是因为这系列的知识比较独立，可以单独拿出来一次专门练习，并且会将之前掌握的知识完全串起来，所以要引起大家足够的重视！
  作业形式：
    本次作业只允许填补代码，不能修改原有代码！
    单独编译每一道题目时，打开注释即可
  作业要求：
    正确编译通过程序，并得到正确的结果
  参考教程：
    http://www.runoob.com/cprogramming/c-tutorial.html
    http://c.biancheng.net/view/241.html
		
  Ubuntu 下打开终端执行：
  gcc ./05_main.c -o main
  ./main

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stdbool.h>
#include <cmath>

#define DEBUG true 

// 定义二维平面上的点
typedef struct _Point {
  char* id;
  int x;
  int y;
} Point;

// 定义二维平面路径
typedef struct _Path {
  char* name;
  Point* key_points; // 该路径上关键点的有序数组
  int key_points_size;
} Path;


char upper(char c)
{
  /*TODO:: 请补全函数*/
  if (c >= 'a' && c <= 'z') {
  	return c - ('a' - 'A');
  }
  return c;
}

int countWords(char* str)
{
  /*TODO:: 可以简单的理解为统计空格数目*/
	int count = 0;
  while(*str != '\0')
  {
  	if(*str == ' ')
  	{
  		count++;
  	}
  	str++;
  }
  return count+1;
}

bool checkWord(char* str, char* word)
{
  /*TODO:: 补全函数，可能会用到 malloc, memcpy 以及 strcmp 函数*/
  char* temp = (char*) malloc(strlen(word));
  bool result = false;
  while(*str)
  {
  	memcpy(temp, str, 3);
  	if(strcmp(temp, word)==0)
  	{
  	  result = true;
  		break;
  	}
  	str++;
  }
	
	free(temp);
	temp = NULL;
	return result;
}

int countWordsFromArray(char** array, int size, char* word)
{
  /*TODO:: 补全函数*/
  
  int count = 0;
  for (int i = 0; i < size; i++)
  {
    if (strcmp(array[i], word)==0) {
      count++;
    }
  }
  
  return count;
}


float computeDistance(Point pt1, Point pt2)
{
  return sqrt((float)(pow(pt1.x-pt2.x, 2) + pow(pt1.y-pt2.y, 2)));
}


float computePathLength(Path path)
{
  /*TODO:: 补全函数*/ 
  float length = 0.f;
  for(int i = 0; i < path.key_points_size-1; i++)
  {
    length += computeDistance(path.key_points[i], path.key_points[i+1]);
  }
  return length;
}


int main()
{
/*
*********************************************************
** TODO::Question1  字符变量
** 注意查阅 ASCII 表
*********************************************************
*/
  char ca = '9';  // 字符变量只占一个字节，只能用单引号
  printf("ca is %c | ca size is %d\n", ca, (int)sizeof(ca));
  printf("ca 对应的 ASCII 十进制为 %d\n", ca);
  
  int ca_int = (int)ca; // 强制转换 ca 为 int 类型 等于对应的 ASCII 值
  printf("ca_int 为 %d\n", ca_int);
  
  unsigned char cb = 255; // 变量范围为0 ~ 255
  printf("cb_int 为 %d\n", cb);
  
  char cd = 'a';
  char cd_upper = 'a' - 32;
  printf("cd_upper 为 %c\n", cd_upper);
  
  // TODO::编写 upper 函数将小写字母转换为大写字母
  char ce = upper('f');
  printf("ce 为 %c\n", ce);

/*
*********************************************************
** TODO::Question2  字符数组及其初始化
*********************************************************
*/
  // 字符串用双引号括住，末尾会以'\0'结尾，但不显示，所以字符串最小的长度为1
  printf("字符串的长度为 %d\n", (int)sizeof(""));
  printf("字符串的长度为 %d\n", (int)sizeof("apple"));
  
  char v1[5];
  v1[0] = 'g';
  v1[1] = 'o';
  v1[2] = 'o';
  v1[3] = 'd';
  v1[4] = '\0'; // 最后一位要补'\0'
  printf("v1 为 %s\n", v1);
  
	char v2[] = "I believe you"; // 字符数组也可以用字符串来初始化
	printf("v2 为 %s\n", v2);
  
  char v3[3];
  memset(v3, 'c', sizeof(v3)); // 全部初始化为 0
  
  for(int i = 0; i < sizeof(v3); i++)
  {
    printf("v3[%d] is %c\n", i, v3[i]);
  }

/*
*********************************************************
** TODO::Question3  字符串指针
*********************************************************
*/
  char* str1 = "Best Regards for You"; // 建议以这样的方式初始化字符串
  printf("str1 is %s\n", str1);
  printf("str1 size is %d\n", strlen(str1)); // 用strlen（）求字符串长度
  
  // TODO::
  int word_number = countWords(str1); // 统计字符串中的单词数
  printf("str1 中的单词数目为 %d\n", word_number);
  
  // TODO::
  bool result = checkWord(str1, "for"); // 检查 "for" 是否在字符串中
  printf("for 是否在 str1 中： %d\n", result);
  
 
  
/*
*********************************************************
** TODO::Question4  字符串数组
*********************************************************
*/
  
  char* person[] = {"wang", "he", "li", "wang"};
  printf("person 数组的长度为 %d \n", (int)(sizeof(person)/sizeof(person[0])));
  
  for(int i = 0; i < (int)(sizeof(person)/sizeof(char*)); i++)
  {
    printf("person[%d] is %s\n", i, person[i]);
  }
  
  // TODO:: 统计 person 数组中 “wang” 的个数
  int number = countWordsFromArray(person, 4, "wang");
  printf("wang 的个数为 %d \n", number);

/*
*********************************************************
** TODO::Question5  字符串格式化
** int sprintf(char *str, const char *format, ...)
*********************************************************
*/
  
  char v9[20];
  sprintf(v9, "Today is a %s day", "good");
  printf("v9 is %s\n", v9);
  
  char v10[20];
  sprintf(v10, "%d-%d-%d", 2018, 11, 2);
  printf("v10 is %s\n", v10);

  printf("-------------------------------------------------\n");
  
/*
*********************************************************
** TODO::Question6  一些综合练习，继续考察结构体以及内存管理
*********************************************************
*/  
  Path path;
  path.name = "path1";
  
  int points_size = 5;
  Point* points = (Point*) malloc(points_size*sizeof(Point));
  for (int i = 0; i < points_size; i++) 
  {
    char id[2];
    sprintf(id, "%d", i);
    points[i].id = id;
    points[i].x = i + 2;
    points[i].y = i + 3;
    printf("points[%d] :\n\t-- id : %s\n\t-- x : %d\n\t-- y : %d\n", i, id, points[i].x, points[i].y);
    printf("*************************\n");
  }
  
  path.key_points = points;
  path.key_points_size = points_size;
  
  // TODO:: 请计算路径 path 的总长度
  float length = computePathLength(path);
  printf("路径 Path 的长度为 %f\n", length);
  
  free(points);
  points = NULL;
  printf("-------------------------------------------------\n");
/*
*********************************************************
** TODO::Question7  位运算
** 不强制要求，作为拓展练习，提交作业时可以把下面的删掉或注释掉
*********************************************************
*/  
  unsigned char a = 255;
  a = 3 & 2;                    // 按位与操作
  printf("a is %d\n", a);
  
  a = 3 | 2;                    // 按位或操作
  printf("a is %d\n", a);
  
  a = 3 ^ 2;                    // 按位异或操作
  printf("a is %d\n", a);
  
  a = 255;
  a = ~a;                       // 按位取反操作
  printf("a is %d\n", a);
  
  a = 3;
  a = a<<3;                     // 左移3位，左移一位相当于乘2, 如果超出边界，则舍弃。
  printf("a is %d\n", a);
  
  a = 64;
  a = a>>3;                     // 右移3位，右移一位相当于除2, 如果超出边界，则舍弃。
  printf("a is %d\n", a);
  
  int b = 15;
  b = b & 0xFF;                 // 取 int 低 8 位
  printf("b is %d\n", b);
  
  // 如果想取一个字节的某一位，通过下面的方式
  // 假如取 第4位
  b = b & (1 << 4);
  printf("b is %d\n", b);       // 如果 b 是 0，那么对应的 bit 就是 0，反之则为 1
  
  // 还可以这样, 取 第4位
  b = 15;
  b = (b >> 4) & 1;
  printf("b is %d\n", b);       // 如果 b 是 1，那么对应的 bit 就是 1，反之则为 0
  
  // 或运算 可以用来设置标志位，会常用到
  
  return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


