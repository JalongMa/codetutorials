/*
  本文件是关于C语言学习的第二份作业，经过了第一份作业的检验，你应该掌握了C语言最基本的知识，包括变量定义，条件判断，循环控制以及简单的数组和函数的用法。
  这次作业目的是检验对于C语言中指针、函数的掌握程度，此次作业相较于第一次作业难度增大许多，请认真查阅学习相关知识点，完成作业！
  作业形式：
    本次作业只允许填补代码，不能修改原有变量定义！本次作业没有陷阱，上次也没有...
  作业要求：
    正确编译通过程序，并得到正确的结果
  参考教程：
    http://www.runoob.com/cprogramming/c-tutorial.html
		
  Ubuntu 下打开终端执行：
  g++ ./02_main.cpp -o main
  ./main

*/

#include <stdio.h>
#include <float.h>

#define DEBUG true 

const bool debug = false; 

float computeMean(float array[], int length) 
{
  /* TODO::在这里填写你的代码*/
  
}

void computeMeanAndVariance(float* array, int length, float* mean, float* variance)
{
  /* TODO::在这里填写你的代码，注意方差计算公式*/

}

void swap(int* a, int* b)
{
  /* TODO::在这里填写你的代码*/

}

void computeHistogram(int* array, const int length, int* vd_element, int* vd_frequence, int* vd_element_number)
{
  /* TODO::在这里填写你的代码*/

}

int main()
{
  /* 指针变量的定义与使用 */
  int var = 20;
  int* a_ptr = NULL;
  int* b_ptr = NULL;
  
  a_ptr = &var;
  
  /* TODO::请补全判断条件*/
  if (!(*a_ptr == )) {
  	printf("答案错误！\n");
  	return 0;
  }

/*
********************************************************
* TODO::Question1
********************************************************
*/

  int va[] = {10, 20, 30, 40}; // 数组名指向数组首地址
  int* c_ptr = va;
  
  /* TODO::请补全printf*/
  printf("va[0] 的地址=%p | va 的地址=%p | c_ptr 的值=%p \n", , , ); // 请观察三者的输出是否一致

  printf("指针变量c_ptr的长度为 %d\n", (int)sizeof(c_ptr)); // 输出指针变量的长度
  printf("数组va中元素的个数为 %d\n", (int)(sizeof(va)/)); // 输出指针变量的长度
  
  // 对数组va求和, /* TODO::请补全 for 循环*/
  int va_sum = 0;
  for (int i = 0; i < sizeof(va)/; i++)
  {
		
  }
  printf("数组va的和是 %d\n", va_sum);
  printf("---------------------------------------------\n");
/*
********************************************************
** TODO::Question2
********************************************************
*/

  float vb[5];
  vb[4] = 50.f;
  float vb_diff = 10.f; // 公差
  float* d_ptr = NULL;
  d_ptr = &vb[4];
  
  /* TODO::已知 vb 是公差为10的等差数列，根据给出的变量定义，利用指针 d_ptr 循环构建等差数列 vb */
	int count = 5;
	while(count > 0) {

	}
	
	float vb_sum = 0;
	for (int i = 0; i < 5; i++)
  {
		printf("vb[%d] is %f\n", i, vb[i]);
		vb_sum += vb[i];
  }
  if (vb_sum != 150.f) {
  	printf("等差数列构建错误，请重试！");
  }
  printf("---------------------------------------------\n");
  
/*
*********************************************************
** TODO::Question3
*********************************************************
*/
	
  int vc[] = {-100, -200, -10000, -244, -255};
  int* e_ptr = vc;
  int vc_max = (int)-DBL_MAX;
  
  //TODO:: 根据已知变量定义，补全while循环，获取vc数组的最大值并赋值给vc_max
  while (e_ptr <= &vc[4]) {
		
  }
  printf("vc_max is %d\n", vc_max);
  printf("---------------------------------------------\n");  
  
/*
*********************************************************
** TODO::Question4    传递数组
*********************************************************
*/
	// 还记得你构建好的等差数组 vb 吗？
	// 请构建函数 computeMean 计算数组vb的平均值
	float mean = computeMean(vb, sizeof(vb)/sizeof(vb[0]));
  printf("数组vb的平均值为 %f\n", mean);
  
  // 请构建函数 computeMeanAndVariance 计算数组vb的均值与方差
  float mean_ = 0.f;
  float variance_ = 0.f;
  computeMeanAndVariance(vb, sizeof(vb)/sizeof(vb[0]), &mean_, &variance_);
  printf("数组vb的均值为 %f | 方差为 %f\n", mean_, variance_);
  
  if (variance_ != 200.f) {
    printf("答案错误！\n");
  	return 0;
  }
  printf("---------------------------------------------\n");
   
/*
*********************************************************
** TODO::Question5    指针形参交换
*********************************************************
*/
  int f1 = 100, f2 = 200;
  //TODO:: 请补全swap函数实现 f1 与 f2 变量的值互换, 即 f1 = 200, f2 = 100
  swap(&f1, &f2);

  printf("f1 的值为 %d | f2 的值为 %d \n", f1, f2);
  printf("---------------------------------------------\n");
  
/*
*********************************************************
** TODO::Question6    二级指针
*********************************************************
*/  
	int g1 = 12;
	int* g1_ptr = &g1;
	int** g2_ptr = &g1_ptr;
	
	*(*g2_ptr) = 34;
	
	//TODO:: 请补全判断条件
  if(!(g1 == ))
  {
    printf("答案错误！");
		return 0;
  }

/*
*********************************************************
** TODO::Question7    指针数组与数组指针
*********************************************************
*/  
  
  int vd[9] = {200, 100, 50, 200, 50, 400, 200, 200, 50};
  int (*vd_ptr)[9] = &vd;
  int* ve[9] = {NULL};
  
  //TODO:: 请给数组 ve 赋值，使 ve 的每个元素指向 vd 对应位置的元素
  for (int i = 0; i < 9; i++) {
		
  }
  printf("数组 ve 的长度为 %d\n", (int)(sizeof(ve)/sizeof(ve[0])));
  
  //TODO:: 请利用 ve 数组求数组 vd 所有元素的和
  int vd_sum = 0;
  for (int i = 0; i < 9; i++) {
		
  }
  printf("数组 vd 的和为 %d\n", vd_sum);
  
  // 请构造函数 computeHistogram() 用来统计数组 vd 中每个元素出现的频次，获得一个元素数组和一个对应的频次的数组，即统计每个元素的重复次数
  // 比如 一个数组 [1,2,3,1,2,3,4]
  // 你应该得到一个元素数组 [1,2,3,4] 和 一个对应的频次数组 [2,2,2,1]
  // 如果你觉得下面的变量定义不太合理，你可以自己实现
  
  int vd_element[9] = {0}; // 元素数组
  int vd_frequence[9] = {0}; // 频次数组
  int vd_element_number = 0; // 有效元素个数，即数组 vd_element 非零元素的个数
  
  computeHistogram(vd, 9, vd_element, vd_frequence, &vd_element_number);
  
  printf("vd_element_number %d\n", vd_element_number);
	
	int element_sum = 0, frequence_sum = 0;
  for(int i = 0; i < vd_element_number; i++)
  {
  	printf("element %d | frequence %d\n", vd_element[i], vd_frequence[i]);
  	
  	element_sum += vd_element[i];
  	frequence_sum += vd_frequence[i];
  }
  
  printf("element_sum %d\n", element_sum);
  printf("frequence_sum %d\n", frequence_sum);
  
  if(element_sum == 750 && frequence_sum == 9) {
    printf("恭喜你完成本次作业！\n");
  } else {
    printf("出错！\n");
  }  
  
	return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


