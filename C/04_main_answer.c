/*
  本文件是关于C语言学习的第四份作业，目的是检验对于C语言中结构体这一重要工具的掌握程度，考虑到大家时间，本次作业难度不会很大，均为结构体基本知识，请务必认真查阅学习相关知识点，完成作业！
  作业形式：
    本次作业只允许填补代码，不能修改原有代码！
    单独编译每一道题目时，打开注释即可
  作业要求：
    正确编译通过程序，并得到正确的结果
  参考教程：
    http://www.runoob.com/cprogramming/c-tutorial.html
    http://c.biancheng.net/view/241.html
		
  Ubuntu 下打开终端执行：
  g++ ./04_main.cpp -o main
  ./main

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

#define DEBUG true 

// 定义 结构体 Point，表示三维连续空间中的一个点
typedef struct _Point
{
	float x;
	float y;
	float z;
} Point;

// 三维空间中的三个点定义一个 Shape
typedef struct _Shape
{
	Point p1;
	Point p2;
	Point p3;
} Shape;

// 各种数据都有，命名为 集合
typedef struct _Set
{
  int a;
  float* b;
  Point* pt;
  int c[3];
} Set;

void printPoint(Point point)
{
	/*TODO:: 利用printf函数打印出point的成员变量的值*/
	printf("x is %f\n", point.x);
	printf("y is %f\n", point.y);
	printf("z is %f\n", point.z);
}

void printPoint_(Point* point)
{
	/*TODO:: 利用printf函数打印出point的成员变量的值*/
	printf("x is %f\n", point->x);
	printf("y is %f\n", point->y);
	printf("z is %f\n", point->z);
}

float computeSum(Point* point_ptr, int size)
{
	/*TODO:: 计算 Point 数组的和*/
  float sum = 0.f;
  for(int i = 0 ; i < size; i++){
    sum += point_ptr->x;
    sum += point_ptr->y;
    sum += point_ptr->z;
  	point_ptr++;
  }
	return sum;
}

Point computerCenter(Shape shape)
{
  Point center;
  center.x = (shape.p1.x + shape.p2.x + shape.p3.x)/3.0;
  center.y = (shape.p1.y + shape.p2.y + shape.p3.y)/3.0;
  center.z = (shape.p1.z + shape.p2.z + shape.p3.z)/3.0;
	return center;
}

int main()
{
/*
*********************************************************
** TODO::Question1  结构体的定义 
*********************************************************
*/
  struct 
  {
    int a;
    float b;
    double c;
  } s1 = {1, 2.f, 3.0}; // s1 是结构体变量，可以直接在定义的时候指定初始值
  
  printf("s1.a = %d | s1.b = %f | s1.c = %lf \n", s1.a, s1.b, s1.c); // 访问结构体成员，获取定义好的结构体成员变量的值
  
  struct _S
  {
    int a;
    float b;
    double c;
  };
  
  struct _S s2 = {1, 2.f, 3.0}; // s2 是结构体变量, "struct _S"可以看做一个类型
  struct _S s3 = {2, 3.f, 4.0}; // s2 与 s3 是两个不同的结构体变量，分别有对应的a,b,c值
  
  // 但是如果定义结构体都要带着 struct 关键字的话，未免过于繁冗，故通常定义结构体需要借助类型重定义 typedef，来重定义结构体类型名，如下：
  typedef struct _List
  {
    int a;
    float b;
    double c;
  }List; // List 为重定义后的类型名，可以用 List 直接声明结构体变量
  // 今后定义结构体均要采用这种方式！你会在大量的C语言项目的源码中看到这样的定义方式
  List l1 = {100, 200.f, 300.0}; // 这样就方便了很多
  printf("l1.a = %d | l1.b = %f | l1.c = %lf \n", l1.a, l1.b, l1.c);
  
  // TODO:: 需要观察一下结构体 List 占用的字节大小，并分析
  printf("l1 size is %d \n", (int)sizeof(l1));
  
/*
*********************************************************
** TODO::Question2  结构体的初始化
** 结构体的初始化也有多种方式，这里都列了出来，目的是大家今后看到都会有印象
*********************************************************
*/
  typedef struct _Group
  {
    int a;
    float b;
    double c;
  }Group;
  
  // 1. 定义时直接顺序赋值
  Group g1 = {1, 2.f, 3.0};
  
  // 2. 定义后逐个赋值
  Group g2;
  g2.a = 1;
  g2.b = 2.f;
  g2.c = 3.0;

/* TODO::请注意，下面这两种方式可能编译过不去，大家可以尝试，在一些纯C语言写的项目中会使用下面的初始化方式

在终端使用 gcc 04_main.c -o main 可以编译通过
   
  // 3. 定义时乱序赋值
  Group g3 = {.b = 2.f, .c = 3.0, .a = 1};
  
  // 4. 另外一种定义时的乱序赋值
  Group g4 = {b : 2.f, c : 3.0, a : 1};
*/

/*
*********************************************************
** TODO::Question3  结构体作为函数参数
*********************************************************
*/
	Point p1 = {1.f, 2.f, 3.f};
	printPoint(p1);

/*
*********************************************************
** TODO::Question4  结构体指针，注意访问成员变量的方式
*********************************************************
*/
	Point p2 = {1.f, 2.f, 3.f};
	Point* p3 = NULL; 
	p3 = &p2;
	printPoint_(p3);

/*
*********************************************************
** TODO::Question5  结构体数组
*********************************************************
*/
	Point vp[3];
	for (int i = 0; i < 3; i++)
	{
	  vp[i].x = 0.f;
	  vp[i].y = 1.f;
	  vp[i].z = 2.f;
	}
	
	//TODO:: 计算数组vp所有点的所有坐标的和
	float sum = computeSum(vp, 3); 
	printf("sum is %f\n", sum);

/*
*********************************************************
** TODO::Question6  结构体嵌套
*********************************************************
*/
  Shape shape1;
  // TODO:: 请定义shape1，三个点的坐标均为 {1.f, 2.f, 3.f}
  Point pt1 = {1.f, 2.f, 3.f};
  Point pt2 = {1.f, 2.f, 3.f};
  Point pt3 = {1.f, 2.f, 3.f};
  shape1.p1 = pt1;
  shape1.p2 = pt2;
  shape1.p3 = pt3;
  
  //TODO:: 请计算 shape1 三维空间中的重心，即x,y,z的平均值
  Point center = computerCenter(shape1);
  printf("center.x = %f | center.y = %f | center.z = %f \n", center.x, center.y, center.z);
  
/*
*********************************************************
** TODO::Question7  多源成员变量结构体
*********************************************************
*/
  Set set;
  // TODO:: 请根据Set的定义为结构体变量 set 随意赋值，并打印出来，答案不会提供这一项，但我会检查大家的答案
  
  
/*
*********************************************************
** TODO::Question8  结构体内存管理与动态分配
*********************************************************
*/  
  Point* pt_ptr = (Point*) malloc(sizeof(Point)); // 只分配一个对象
  pt_ptr->x = 1.f;
  pt_ptr->y = 2.f;
  pt_ptr->z = 3.f;
  free(pt_ptr);
  pt_ptr = NULL;
  
  Point* vpt_ptr = (Point*) malloc(3*sizeof(Point)); // 分配了3个对象
  for(int i = 0; i < 3; i++)
  {
  	// TODO:: 请初始化分配的内存区域，随意初始化
  	vpt_ptr->x = 1.f;
  	vpt_ptr->y = 2.f;
  	vpt_ptr->z = 3.f;
  	vpt_ptr++;
  }
  vpt_ptr -= 3;
  free(vpt_ptr);
  vpt_ptr = NULL;
  
  return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


