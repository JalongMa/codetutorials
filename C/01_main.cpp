/*
	本文件是关于C语言学习的第一份作业，主要检验C语言最基本的语法规则，非常非常简单！
	作业形式：
		主要以填补代码的方式检验C语言的学习效果，可能会有部分代码片段为故意设置的错误陷阱，不改正的话，会导致无法正常编译通过，请注意！
	作业要求：
		正确编译通过程序，并得到正确的结果
	参考教程：
		http://www.runoob.com/cprogramming/c-tutorial.html
		
	Ubuntu 下打开终端执行：
	g++ ./01_main.cpp -o main
	./main

*/

#include <stdio.h>


float function1(float x, float y)
{
	return x > y;
}

float function2(float x, float y)
{
	/* TODO::在这里填写你的代码 */
	
	
}



int main()
{
	/* 变量定义、基本运算 */
	int a = 2, b = 3, d = 5, e = 8;
	float fa = 4.f, fb = 5.f;
	double da = 10.0, db = 20.0;
	char c = 'C';
	
	// 请补全下面的 printf 函数
	printf("a is %d | b is %d\n", a, b);
	printf("fa*a is %f | fb*b is %f\n", , );
	printf("da/a is %lf | db/d is %lf\n", , );
	printf("8/5 is %f\n", );
	printf("c is %c\n", );
	printf("d%b is \n", ); // 求余数
	
	printf("--------------------\n");
	
/*********************************************************/
/*********************************************************/

	/* 条件判断 */
	// 请补全判断条件
	if () {
		printf("a < b\n");
	} else if (){
		printf("a == b\n");
	} else {
		printf("a > b\n");
	}
	
	if () {
		printf("a 是 偶数\n");
	} else {
		printf("a 是 奇数\n");
	}
	
	// 请利用三元运算符获取a的符号
	int sign;
	 ? : ; 
	printf("sign is %d\n", sign);
	
	// 请仔细研究switch的控制逻辑，以及break的作用
	char c1 = 'P';
	switch(c1)
	{
		case 'A' :
			printf("c1 is A！\n" );
			break;
		case 'L' :
			printf("c1 is L！\n" );
			break;
		case 'O' :
		case 'P' :
			printf("c1 is O or P！\n" );
			break;
		default :
			printf("None\n" );
			break;
	}
	
	printf("--------------------\n");
/*********************************************************/
/*********************************************************/
	
	/* for 循环、while 循环 */
	for( int i = 0; i < 8; i++ )
	{
		printf("$");
	}
	printf("\n");
	
	// 请利用for循环打印出 "@@####@@"
	for( int i = 0; i < 8; i++ )
	{
		/* TODO::在这里填写你的代码 */
	}
	printf("\n");
	
/*
请利用for循环打印出如下右对齐等边三角形图案

     *
    **
   ***
  ****
 *****
******

*/
	int rows = 6;
	for(int i=0; i<rows; i++)
	{
		/* TODO::在这里填写你的代码 */
	}
	
	printf("--------------------\n");
	
	// 请添加两个 if 判断条件 使得最终仅仅输出 count is 998
	int count = 0;
	while(count < 1000) {
		count++;
		/* TODO::补全两个if条件 */
		if () {
		}
		if () {
		}
		printf("count is %d\n", count);
	}
	
	printf("--------------------\n");
/*********************************************************/
/*********************************************************/
	/* 数组 */
	// 注意数组的初始化方式
	int v[10];
	double v1[5] = {1000.0, 2.0, 3.4, 7.0, 50.0};
	float v2[] = {10.0f, 12.0f, 3.4f, 17.0f, 250.0f};
	char vc[] = {'A', 'p', 'p', 'l', 'e'};
	
	// 给数组循环赋值
	for (int i = 0; i < 10; i++) {
		v[i] = i;
	}
	
	// 请利用vc数组打印出 Apple
	for (int i = 0; i < 5; i++) {
		/*在这里填写你的代码*/
		
	}
	printf("\n");
	
	/* 函数 */
	// 调用function1函数 比较两个数的大小
	for (int i = 0; i < 5; i++) {
		float result = function1((float)v1[i], v2[i]);
		printf("results is %f\n", result);
	}
	
	// 数组 v1 与 v2 可看做多维向量（5维）
	// 请实现向量 2×(v1) 与 3x(v2) 的点积运算，将最终结果赋值给 sum
	// 请设计 function2 函数，实现 a*x + b*y 运算
	float sum = 0.f;
	for (int i = 0; i < 5; i++) {
		/* TODO::在这里填写你的代码 */
		// 请调用所设计的 function2 函数

	}
	printf("sum is %f\n", sum);
	
	printf("--------------------\n");
	if(sum==3002.0) {
		printf("恭喜你完成本次作业！\n");
	} else {
		printf("出错！\n");
	}
	
	return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


