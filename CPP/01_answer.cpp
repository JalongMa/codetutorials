/*
	本文件是关于C++语言学习的第一份作业，直接从类的使用展开！
	作业形式：
		主要以填补代码的方式检验C++语言的学习效果！
	作业要求：
		正确编译通过程序，并得到正确的结果
	参考教程：
		https://www.w3cschool.cn/cpp/cpp-classes-objects.html
		
	Ubuntu 下打开终端执行：
	g++ ./01_main.cpp -o main
	./main

*/

#include <iostream>
#include <cmath>
#include <math.h>

using namespace std;

class Student
{
  public:
    string id;
    string name;
    int age;
    float height;
    float weight;
};

class Point
{
	public:
	  int x;
	  int y;
};

// 线段
class Line
{
	public:
	  Point pt1;
	  Point pt2;
	  
	  float getLength() {
	  // TODO:: 返回线段的长度，补全函数
	    return sqrt(pow(pt1.x-pt2.x, 2) + pow(pt1.y-pt2.y, 2));
	  }
	  
};

// 矩形
class Rectangle
{
  public:
  
    void setPt1(Point pt) {
    	pt1 = pt;
    }
    
    void setPt2(Point pt) {
      pt2 = pt;
    }
    
    Point getPt1() {
    	return pt1;
    }
    
    Point getPt2() {
    	return pt2;
    }
    
    int getPerimeter() {
      return 2*(pt2.x-pt1.x) + 2*(pt2.y-pt1.y);  
    }
    
    int getArea() {
      return (pt2.x-pt1.x)*(pt2.y-pt1.y);
    }
    
    int getWidth() {
      return pt2.x-pt1.x;
    }
    
    int getHeight() {
    	return pt2.y-pt1.y;
    }
    
  private:
    Point pt1;   // 私有成员变量
    Point pt2;   // 私有成员变量
};

// 圆
class Circle
{
  public:
    // 普通带参数构造函数
    Circle(float r) {
      this->r = r;
    }
    
    float getArea() {
      return M_PI*r*r;
    }
    
    // 析构函数
    ~Circle(){}
  private:
    float r;
};

// 立方体
class Box
{
  public:
    // 构造函数初始化列表
    Box(int length, int width, int height):length(length), width(width), height(height) {}
    
    Box():length(0), width(0), height(0) {}
    
    int getVolume() {
      return length*width*height;
    }
    
    ~Box(){}
    
  private:
    int length;
    int width;
    int height;
};

// 简单两位数计算器
class Calculator
{
  public:
    Calculator(int x, int y) {
      this->x = x;
      this->y = y;
    }
    
    int add() {
      return this->x + this->y;
    }
    
  private:
    int x;
    int y;
};

// 引用作为函数的参数
void swap(int& v1, int& v2)
{
  int temp = v1;
  v1 = v2;
  v2 = temp;
}

int main()
{
/*
*********************************************************
** TODO::Task1  类的简单定义与使用: 成员变量
*********************************************************
*/  
  Student stu;
  stu.id = "SY1513311";
  stu.name = "Jalong Ma";
  stu.age = 26;
  stu.height = 1.76;
  stu.weight = 55;
  cout << stu.name << " 的体质指数BMI是：" << stu.weight/(stu.height*stu.height) << endl;
  
  // TODO:: 请在下面计算你自己的 BMI 指数，看看是不是正常？ ^_^
  // 
  
/*
*********************************************************
** TODO::Task2  类的简单定义与使用: 成员函数
*********************************************************
*/    
  Line line;
  Point pt1, pt2;
  pt1.x = 1; pt1.y = 1;
  pt2.x = 2; pt2.y = 2;
  line.pt1 = pt1; line.pt2 = pt2;
  
  cout << "line 的长度为 " << line.getLength() << endl;

/*
*********************************************************
** TODO::Task3  类的简单定义与使用: 权限控制
*********************************************************
*/

  Rectangle rect;
  Point r_pt1, r_pt2;
  r_pt1.x = 1; r_pt1.y = 1;
  r_pt2.x = 3; r_pt2.y = 3;
  rect.setPt1(r_pt1);
  rect.setPt2(r_pt2);
  
  cout << "rect 的宽度为： " << rect.getWidth() << endl;
  cout << "rect 的高度为： " << rect.getHeight() << endl;
  cout << "rect 的周长为： " << rect.getPerimeter() << endl;
  cout << "rect 的面积为： " << rect.getArea() << endl;
  
/*
*********************************************************
** TODO::Task4  类的简单定义与使用: 构造函数与析构函数
*********************************************************
*/  
  Circle circle(5.f);
  cout << "circle 的面积为： " << circle.getArea() << endl;
  
  Box box1;
  Box box2(1, 2, 3);
  
  cout << "box1 的体积为： " << box1.getVolume() << endl;
  cout << "box2 的体积为： " << box2.getVolume() << endl;
  
  box1 = Box(2, 3, 4); // 赋值操作符重载函数被调用
  cout << "box1 的体积变为： " << box1.getVolume() << endl;
  
  box1 = box2;  // 赋值操作符重载函数被调用
  cout << "box1 的体积变为： " << box1.getVolume() << endl;
  
  Box box3(box2); // 拷贝构造函数被调用
  cout << "box3 的体积变为： " << box3.getVolume() << endl;
  
  Box box4 = box2; // 拷贝构造函数被调用
  cout << "box4 的体积变为： " << box4.getVolume() << endl;

/*
*********************************************************
** TODO::Task5  类的简单定义与使用: this指针
** this指针用来在类内部访问类的成员变量与成员函数
*********************************************************
*/  
  Calculator calc(5, 6);
  cout << "calc.add() 为： " << calc.add() << endl;
/*
*********************************************************
** TODO::Task6  类的简单定义与使用: 对象指针
*********************************************************
*/
  Box* box_ptr = NULL;
  box_ptr = &box2;
  cout << "box_ptr 的体积变为： " << box_ptr->getVolume() << endl;
  
  cout << "-----------------------------------------------" << endl;
/*
*********************************************************
** TODO::Task7  C++ 引用
*********************************************************
*/
  int a = 3;
  int& b = a;
  cout << "b 的值为 " << b << endl;
  cout << "a 在内存中的地址为 " << &a << endl;
  cout << "b 在内存中的地址为 " << &b << endl; // 可以看出，内存中的地址值是相同的
  
  int* ptr = &a;
  int*& ptr_new = ptr; // 对指针变量的引用 语法：类型 *&引用名=指针名
  cout << &ptr << "    " << &ptr_new << endl; // 观察二者的地址是否相同
  
  //TODO:: 利用引用完成swap交换函数
  int value1 = 10, value2 = 20;
  swap(value1, value2);
  
  cout << "value1 的值为 " << value1 << endl;
  cout << "value2 的值为 " << value2 << endl;

/*
  1.在引用的使用中，单纯给某个变量去别名是毫无意义的，引用的目的主要用于在函数参数的传递中，解决大块数据或对象的传递效率和空间不如意的问题

2.用引用传递函数的参数，能保证参数在传递的过程中不产生副本，从而提高传递效率，同时通过const的使用，还可以保证参数在传递过程中的安全性

3.引用本身是目标变量或对象的别名，对引用的操作本质上就是对目标变量或对象的操作。因此能使用引用时尽量使用引用而非指针 
*/
 
  return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


