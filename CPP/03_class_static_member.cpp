/*
	本文件主要关注C++类关于static静态成员变量以及静态成员函数的一些知识！
	作业形式：
		主要以填补代码的方式检验C++语言的学习效果！
	作业要求：
		正确编译通过程序，并得到正确的结果
	参考教程：
		https://www.cnblogs.com/raichen/p/4752025.html
		
	Ubuntu 下打开终端执行：
	g++ ./01_main.cpp -o main
	./main

*/

#include <iostream>
#include <stdio.h>
#include <cmath>
#include <math.h>
#include <string.h>

using namespace std;

class Tracker
{
public:
  Tracker() {
  	count++;
  }
  
  ~Tracker(){}

	// 静态成员函数只能调用静态成员变量
  static void createNewInstance() {
  	count++;
  }
	
	int getCount() {
		printf("-----count is %d-----\n", count);
		return count;
	}
	
private:
  static int count;    // 静态成员变量

};

// 注意：static类静态成员变量初始化必须要在类外全局区进行初始化
int Tracker::count = 0;

int main()
{
/*
*********************************************************
** TODO::Task1  静态成员变量与静态成员函数
** 静态成员变量的最大作用在于所有类对象共享内存
*********************************************************
*/
	
	Tracker tracker;
	Tracker::createNewInstance();  // 类调用静态成员函数
	tracker.getCount();
	
  return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


