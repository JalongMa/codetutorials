/*
	本文件主要关注C++类关于构造函数的一些知识！
	[1] 默认构造函数
	[2] 拷贝构造函数
	[3] 重载赋值操作符
	作业形式：
		主要以填补代码的方式检验C++语言的学习效果！
	作业要求：
		正确编译通过程序，并得到正确的结果
	参考教程：
		https://www.cnblogs.com/raichen/p/4752025.html
		
	Ubuntu 下打开终端执行：
	g++ ./01_main.cpp -o main
	./main

*/

#include <iostream>
#include <stdio.h>
#include <cmath>
#include <math.h>
#include <string.h>

using namespace std;

// 立方体
class Box
{
  public:
    // 构造函数初始化列表
    Box(int length, int width, int height):length(length), width(width), height(height) {}
    
    Box():length(0), width(0), height(0) {}
    
    // 显式指定拷贝构造函数
    Box(const Box& ref) : length(ref.length), width(ref.width), height(ref.height) {}
    
    
    // 等号运算符重载
    // 注意，这个类似复制构造函数，将=右边的本类对象的值复制给等号左边的对象，它不属于构造函数，等号左右两边的对象必须已经被创建
    // 若没有显式地写=运算符重载，则系统也会创建一个默认的=运算符重载，只做一些基本的拷贝工作
    Box& operator=(const Box& ref) {
      printf("-----被调用-----\n");
			// 首先检测等号右边的是否就是左边的对象本，若是本对象本身,则直接返回
			if ( this == &ref ) 
			{
				return *this;
			}
			
			this->length = ref.length;
			this->width = ref.width;
			this->height = ref.height;
      
      return *this;
    }   
    
    int getVolume() {
      return length*width*height;
    }
    
    void printDetails() {
      printf("l : %d | w : %d | h : %d\n", length, width, height);
    }
    
    ~Box(){}
    
  private:
    int length;
    int width;
    int height;
};


class Person
{
public:

  Person() {
  
  }
  
  Person(char* pName) {
    printf("---Person(char* pName)--被调用-----\n");
    m_pName = new char[strlen(pName)+1];
    if(m_pName != NULL) {
      strcpy(m_pName, pName);
    }
  }
  
  // 显式实现拷贝构造函数实现深拷贝
  Person(const Person& person) {
    this->m_pName = new char[strlen(person.m_pName)+1];
    if(this->m_pName) {
      strcpy(this->m_pName, person.m_pName);
    }
  }
  
  // 显式实现等号运算符重载
  Person& operator=(const Person& person) {
    
    printf("---Person& operator=(const Person& person)函数被调用-----\n");
		// 首先检测等号右边的是否就是左边的对象本，若是本对象本身,则直接返回
		if ( this == &person ) 
		{
			return *this;
		}
		
    this->m_pName = new char[strlen(person.m_pName)+1];
    if(this->m_pName) {
      strcpy(this->m_pName, person.m_pName);
    }
    
    return *this;
  }
  
  ~Person() {
    printf("---~Person()--析构函数被调用-----\n");
    if (m_pName) {
    	delete m_pName;
    }
  }

private:
  // Person(const Person& person); // 如果想避免调用默认的拷贝构造函数，可以声明为私有类型，可以不用实现函数，这样编译会报错，从而避免该问题
  char* m_pName;
};

// 下面两个类用来解释explicit关键字
class Point
{
public:
  
  Point() {}
  
  Point(int x, int y = 5) {
    this->x = x;
    this->y = y;
  }
  
  ~Point() {
  }

  int x;
  int y;
};

class Point2
{
public:
  
  Point2() {}
  
  explicit Point2(int x, int y = 5) {
    this->x = x;
    this->y = y;
  }
  
  ~Point2() {
  }

  int x;
  int y;
};

int main()
{
/*
*********************************************************
** TODO::Task1  成员变量初始化推荐使用初始化列表的方式
*********************************************************
*/
  Box box1;               // 调用无参构造函数
  Box box2(1, 2, 3);      // 调用有参构造函数
  Box box5 = Box(5, 5, 5); // 相当于上面的方式
  
  Box box3 = box2;        // 调用拷贝构造函数
  Box box4(box3);           // 调用拷贝构造函数
  
  box1.printDetails();
  box2.printDetails();
  box3.printDetails();
  box4.printDetails();
  
/*
以下几种情况都会自动调用拷贝构造函数：
1）用一个已有的对象初始化一个新对象的时候
2）将一个对象以值传递的方式传给形参的时候
3）函数返回一个对象的时候
*/

  box4 = box5;         // 调用等号重载操作符
  box4.printDetails();  

/*
*********************************************************
** TODO::Task2  
** 当类的成员变量在构造函数中需要开辟新的内存空间时
** 必须显式重写复制构造函数与重载等号操作符才能达到复制的效果（深拷贝）
*********************************************************
*/
  Person man1("Jalong");
  Person man2(man1); //如果没有显式重写复制构造函数，会将指针delete两次，报错
  Person man3("Robin");
  
  man3 = man2;
  
  Person man10;
  man10 = Person("Alex"); // 调用等号重载操作符，因为等号两边对象都已经被创建
	printf("---------------------------------------------\n");
/*
*********************************************************
** TODO::Task3 
** explicit 关键字声明的显式构造函数
** 主要针对单参数构造函数，抑制内置类型隐式转换
1.C++中的隐式构造函数
　　如果c++类的其中一个构造函数有一个参数，那么在编译的时候就会有一个缺省的转换操作：将该构造函数对应数据类型的数据转换为该类对象。
2.Explicit Constructors显式构造函数
　　为了避免上面提到的只有一个参数的构造函数采用的缺省转换操作，在构造函数前，使用Explicit 关键字修饰即可。
*********************************************************
*/
  Point pt1;
  pt1 = 10;  // 发现直接给pt1这个类对象赋值10，会间接调用构造函数Point(int x, int y = 5)，相当于 pt1 = Point(10)
  // 由此可知:当类构造函数的参数只有一个的时候,或者所有参数都有默认值的情况下,类A的对象时可以直接被对应的内置类型隐式转换后去赋值的,这样会造成错误,应当避免这样的情况发生
  
  printf("x is %d | y is %d\n", pt1.x, pt1.y);
  
  Point2 pt2(10);
  // pt2 = 10;  // 这样会报错，因为我们对构造函数声明了explicit关键字，explicit可以抑制内置类型隐式转换
  return 0;
}


/*
********************************************************
* TODO::请评价此次检测作业难度，10分为非常非常难
* 你的评分是：
********************************************************
*/


